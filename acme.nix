{ lib, pkgs, config, ...}:
with lib; let
  cfg = config.mewp-nixos.acme;
  domains = unique  (map (removePrefix ".*") (flatten (mapAttrsToList (k: v: v ++ [k]) cfg)));
  dnsZones = listToAttrs (map (hostname: {
    name = "_acme-challenge.${hostname}";
    value = {
      extraConfig = "allow-update { key rfc2136key.example.com.; };";
      SOA = {
        nameServer = "${hostname}";
        adminEmail = "hostmaster@${hostname}";
        serial = 1;
      };

      NS = [
        "${hostname}."
      ];
    };
  }) (traceVal domains));
in {
  options.mewp-nixos.acme = mkOption {
    type = types.attrsOf (types.listOf types.str);
    default = {};
  };

  config = {
    mewp.dns.zones = dnsZones;
    security.acme.certs = mapAttrs (name: domains: {
        domain = name;
        dnsProvider = "rfc2136";
        credentialsFile = "/var/lib/secrets/certs.secret";
        # We don't need to wait for propagation since this is a local DNS server
        dnsPropagationCheck = false;
        extraDomainNames = domains;
    }) cfg;
  };
}
