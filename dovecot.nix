{ pkgs, lib, config, ... }: let
  inherit (lib) mkIf listToAttrs mapAttrsToList concatStringsSep mkOption mkEnableOption types optionalAttrs mkRenamedOptionModule optionalString;
  cfg = config.mewp-nixos.dovecot;

  socketsToString = mapAttrsToList (path: config: ''
    unix_listener ${path} {
      mode = ${config.mode}
      user = ${config.user}
      group = ${config.group}
    }'');
  services = mapAttrsToList (service: sockets: ''
    service ${service} {
      ${concatStringsSep "\n" (socketsToString sockets)}
    }
    '') cfg.sockets;

  dovecotSql = pkgs.writeText "sql.conf" ''
    driver = pgsql
    connect = dbname=${cfg.db.name}
    default_pass_scheme = CRYPT
    password_query = SELECT "${cfg.db.emailField}" as user, "${cfg.db.passwordField}" AS password FROM ${cfg.db.table} WHERE "${cfg.db.emailField}" = '%u'
    user_query = SELECT 1 FROM ${cfg.db.table} WHERE "${cfg.db.emailField}" = '%u'
  '';

  sqlDbConfig = optionalString cfg.sqlDb.enable ''
      passdb {
        driver = sql
        args = ${dovecotSql}
      }

      userdb {
        driver = sql
        args = ${dovecotSql}
      }
  '';
in
{
  options.mewp-nixos.dovecot = {
    enable = lib.mkEnableOption "dovecot";
    acmeHost = mkOption {
      type = types.nullOr types.str;
      default = null;
    };

    sockets = mkOption {
      type = with lib.types; attrsOf (attrsOf (submodule {
        options = {
          mode = mkOption {
            type = str;
            default = "0660";
          };

          user = mkOption {
            type = str;
          };

          group = mkOption {
            type = str;
          };
        };
      }));
    };

    sqlDb = {
      enable = mkOption {
        type = types.bool;
        default = true;
        description = "Enable using sql passdb and userdb";
      };

      name = mkOption {
        type = types.str;
        default = "mail";
        description = "Name of the database containing the user data";
      };

      table = mkOption {
        type = types.str;
        default = "users";
        description = "Name of the table containing the user data";
      };

      emailField = mkOption {
        type = types.str;
        default = "email";
        description = "Name of the field containing the user's email";
      };

      passwordField = mkOption {
        type = types.str;
        default = "password";
        description = "Name of the field containing the user's password";
      };
    };
  };

  imports = [
    (mkRenamedOptionModule [ "mewp-nixos" "dovecot" "db" ] [ "mewp-nixos" "dovecot" "sqlDb" ])
  ];

  config = mkIf cfg.enable {
    nixpkgs.config.packageOverrides = super: optionalAttrs cfg.sqlDb.enable {
      dovecot = super.dovecot.override { withPgSQL = true; };
    };

    security.acme.certs."${cfg.acmeHost}".postRun = mkIf (cfg.acmeHost != null) "systemctl reload postfix dovecot2";

    services.postgresql = mkIf cfg.sqlDb.enable {
      enable = true;
      package = pkgs.postgresql_16;
      ensureUsers = [
        { name="root"; }
      ];
      ensureDatabases = [ "mail" ];
    };

    services.dovecot2 = {
      enable = true;
      enablePAM = false;
      enableLmtp = true;
      createMailUser = true;
      mailUser = "mail";
      mailGroup = "mail";
      mailPlugins.perProtocol.imap.enable = ["imap_sieve"];
      mailPlugins.perProtocol.lmtp.enable = ["sieve"];

      mailboxes = listToAttrs (
        map (name: { inherit name; value = { auto = "subscribe"; specialUse = name; }; }) [ "Trash" "Drafts" "Sent" "Junk" ]
      );

      sieve.extensions = [ "fileinto" ];
      sieve.scripts = {
        after = builtins.toFile "spam.sieve" ''
          require [ "fileinto" ];

          if header :is "X-Spam" "Yes" {
              fileinto "Junk";
              stop;
          }
        '';
      };

      extraConfig =
      ''
      mail_home = /var/spool/mail/%u/home
      lda_mailbox_autosubscribe = yes
      lda_mailbox_autocreate = yes

      plugin {
        sieve = file:/var/sieve/%u/scripts;active=/var/sieve/%u/active.sieve
        sieve_default = file:/var/sieve/%u/default.sieve
        sieve_default_name = default
      }

      ${concatStringsSep "\n" services}
      ${sqlDbConfig}
      '';
    } // optionalAttrs (cfg.acmeHost != null) {
      sslServerKey = "/var/lib/acme/${cfg.acmeHost}/key.pem";
      sslServerCert = "/var/lib/acme/${cfg.acmeHost}/cert.pem";
      sslCACert = "/var/lib/acme/${cfg.acmeHost}/chain.pem";
    };
  };
}
