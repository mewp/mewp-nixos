{ lib, pkgs, config, ...}:

with lib;
with import ./lib.nix { inherit pkgs lib; };
let
  cfg = config.services.dovecot2.sieve;

  sieveScript = name: value: {
    name = "${name}.sieve";
    value = value.sieve;
  };

  pipeDir = writeFiles "sieve_pipe_bin_dir" cfg.pipeScripts;
in {
  options.services.dovecot2.sieve = {
    enable = mkEnableOption "Dovecot2 sieve";

    pipeScripts = mkOption {
      type = with types; attrsOf str;
      default = {};
    };

    extraConfig = mkOption {
      type = types.lines;
      default = "";
    };
  };

  config = {
    services.dovecot2 = {
      modules = [ pkgs.dovecot_pigeonhole ];
      protocols = [ "sieve" ];

      extraConfig = ''
      plugin {
        sieve_plugins = sieve_imapsieve ${optionalString (cfg.pipeScripts != {}) "sieve_extprograms"}

        ${cfg.extraConfig}

        ${optionalString (cfg.pipeScripts != {}) "sieve_pipe_bin_dir = ${pipeDir}"}
        ${optionalString (cfg.pipeScripts != {}) "sieve_global_extensions = +vnd.dovecot.pipe +vnd.dovecot.environment"}
      }
      '';
    };
  };
}
