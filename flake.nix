{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.dns = {
    url = "github:kirelagin/dns.nix";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, dns, flake-utils }: let
    nixosModules.dns = ./dns.nix;
    nixosModules.mailserver = import ./mailserver.nix { inherit dns; };
    nixosModules.acme = ./acme.nix;
    nixosModules.caldav = ./caldav.nix;
    nixosModules.default = {
      imports = [
        ./dns.nix
        (import ./mailserver.nix { inherit dns; })
        ./acme.nix
        ./caldav.nix
      ];
    };
  in {
    inherit nixosModules;
    overlay = final: prev: {
      rebuild = prev.writeShellScriptBin "rebuild" ''cd /etc/nixos; sudo nix flake update; sudo nixos-rebuild "$@"'';
      radicale-dovecot-auth = prev.python3Packages.callPackage ./pkgs/radicale-dovecot-auth.nix {};
    };
    # nixosModule = import ./mailserver.nix { inherit dns; };
  } // flake-utils.lib.eachSystem [ "x86_64-linux" ] (system: let
    pkgs = import nixpkgs {
      inherit system;

      overlays = [ self.overlay ];
    };
  in rec {
    # packages.rebuild = pkgs.rebuild;
    checks.dns = pkgs.nixosTest (import ./tests/dns.nix { inherit nixosModules; });
    packages.radicale-dovecot-auth = pkgs.radicale-dovecot-auth;
  });
}
