{ lib, stdenv
, buildPythonPackage
, fetchPypi
, radicale
}:

buildPythonPackage rec {
  pname = "radicale_dovecot_auth";
  version = "0.4.1";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256:1jbhiw1dwh6fz5ygjgp2kgwvaqghxmj65j1jy4im3rv3dnsjmb2x";
  };

  buildInputs = [ radicale ];

  doCheck = false;
}
