{ test }:
let
  flake = builtins.getFlake "path:.";
in flake.checks.${builtins.currentSystem}.${test}.driverInteractive
# let
#   test = import <nixpkgs/nixos/tests/make-test-python.nix> {
#     machine = { config, pkgs, ... }: {
#       imports = [ ./dns.nix ];
#       environment.systemPackages = with pkgs; [ bind.dnsutils ];
#
#       mewp.dns.zones."example.org." = {
#         SOA = {
#           nameServer = "ns.example.org.";
#           adminEmail = "admin@example.org.";
#           serial = 0;
#         };
#
#         NS = [ "ns" ];
#         ns.A = [ "127.0.0.1" ];
#       };
#     };
#
#     testScript = ''
#       machine.wait_for_unit("bind.service")
#       assert "status: NOERROR" in machine.execute("dig @127.0.0.1 SOA example.org")[1]
#     '';
#   };
# in (test {
#   pkgs = import <nixpkgs> {};
#   system = builtins.currentSystem;
# }).driverInteractive
