{ lib, pkgs, config, ...}:

with lib;
with import ./lib.nix { inherit pkgs lib; };
let
  cfg = config.mewp-nixos.mailserver;

  learn_ham_sieve = ''
    require ["vnd.dovecot.pipe", "copy", "imapsieve", "environment", "variables"];

    if environment :matches "imap.mailbox" "*" {
      set "mailbox" "''${1}";
    }

    if string "''${mailbox}" "Trash" {
      stop;
    }

    if environment :matches "imap.user" "*" {
      set "username" "''${1}";
    }

    pipe :copy "learn-ham" [ "''${username}" ];
  '';

  learn_spam_sieve = ''
    require ["vnd.dovecot.pipe", "copy", "imapsieve", "environment", "variables"];

    if environment :matches "imap.user" "*" {
      set "username" "''${1}";
    }

    pipe :copy "learn-spam" [ "''${username}" ];
  '';

  sieveDir = writeFiles "sieve_scripts_dir" {
    "learn-ham.sieve" = learn_ham_sieve;
    "learn-spam.sieve" = learn_ham_sieve;
  };

in {
  options.mewp-nixos.mailserver.rspamd = {
    enable = mkEnableOption "rspamd";

    banMailingLists = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.rspamd.enable {
    services.dovecot2.sieve = {
      enable = true;
      pipeScripts = {
        learn-ham = "exec ${pkgs.rspamd}/bin/rspamc -h /run/rspamd/worker-controller.sock learn_ham";
        learn-spam = "exec ${pkgs.rspamd}/bin/rspamc -h /run/rspamd/worker-controller.sock learn_spam";
      };

      extraConfig = ''
        imapsieve_mailbox1_name = Junk
        imapsieve_mailbox1_causes = COPY
        imapsieve_mailbox1_before = file:${sieveDir}/learn-spam.sieve

        imapsieve_mailbox2_name = *
        imapsieve_mailbox2_from = Junk
        imapsieve_mailbox2_causes = COPY
        imapsieve_mailbox2_before = file:${sieveDir}/learn-ham.sieve
      '';
    };

    services.rspamd = {
      enable = true;

      locals = {
        "milter_headers.conf" = { text = ''
            extended_spam_headers = yes;
        ''; };
        "redis.conf" = { text = ''
            servers = "127.0.0.1:6379";
        ''; };
        "classifier-bayes.conf" = { text = ''
            cache {
              backend = "redis";
            }
        ''; };
        "dkim_signing.conf" = { text = ''
            enabled = false;
        ''; };
      } // (optionalAttrs cfg.rspamd.banMailingLists {
        "headers_group.conf" = { text = ''
          symbols {
            HAS_LIST_UNSUB {
              score = 6;
            }
          }
        ''; };
      });

      workers.rspamd_proxy = {
        type = "rspamd_proxy";
        bindSockets = [{
          socket = "/run/rspamd/rspamd-milter.sock";
          mode = "0664";
        }];
        count = 1; # Do not spawn too many processes of this type
        extraConfig = ''
          milter = yes; # Enable milter mode
          timeout = 120s; # Needed for Milter usually

          upstream "local" {
            default = yes; # Self-scan upstreams are always default
            self_scan = yes; # Enable self-scan
          }
        '';
      };

      workers.controller = {
        type = "controller";
        count = 1;
        bindSockets = [{
          socket = "/run/rspamd/worker-controller.sock";
          mode = "0666";
        }];
        includes = [];
        extraConfig = ''
          static_dir = "''${WWWDIR}"; # Serve the web UI static assets
        '';
      };
    };

    services.redis.servers."".enable = true;
    services.redis.servers."".bind = "127.0.0.1";

    systemd.services.rspamd = {
      requires = [ "redis.service" ];
      after = [ "redis.service" ];
    };

    systemd.services.postfix = {
      after = [ "rspamd.service" ];
      requires = [ "rspamd.service" ];
    };

    users.extraUsers.postfix.extraGroups = [ config.services.rspamd.group ];
  };
}
