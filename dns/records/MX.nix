{ lib }:
with lib; {
  options = {
    priority = mkOption {
      type = types.int;
    };

    value = mkOption {
      type = types.str;
      example = "mx.example.org.";
    };
  };

  fromString = value: let
    split = splitString " " value;
  in {
    priority = toInt (head split);
    value = head (tail split);
  };
  __toString = { priority, value }: "IN MX ${toString priority} ${value}";
}
