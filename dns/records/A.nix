{ lib }:
with lib; {
  options = {
    ipAddress = mkOption {
      type = types.str;
      example = "127.0.0.1";
    };
  };

  fromString = ipAddress: { inherit ipAddress; };
  __toString = self: "IN A ${self.ipAddress}";
}
