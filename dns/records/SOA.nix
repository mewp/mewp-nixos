{ lib }:
with lib; {
  options = {
    nameServer = mkOption {
      type = types.str;
      example = "ns1.example.org";
    };

    adminEmail = mkOption {
      type = types.str;
      example = "admin@example.org";
      apply = s: replaceStrings ["@"] ["."] s;
    };

    serial = mkOption {
      type = types.nullOr types.ints.u32;
      example = 2137;
      description = "The serial number of the zone. If null, it will be generated automatically based on the current generation number.";
    };

    refresh = mkOption {
      type = types.ints.u32;
      default = 24 * 60 * 60;
    };

    retry = mkOption {
      type = types.ints.u32;
      default = 600;
    };

    expire = mkOption {
      type = types.ints.u32;
      default = 10 * 24 * 60 * 60;
    };

    minimum = mkOption {
      type = types.ints.u32;
      default = 10 * 24 * 60 * 60;
    };
  };

  __toString = self: with self; "IN SOA ${nameServer}. ${adminEmail}. (${toString serial} ${toString refresh} ${toString retry} ${toString expire} ${toString minimum})";
}
