{ lib }:
with lib; {
  options = {
    ipAddress = mkOption {
      type = types.str;
      example = "::1";
    };
  };

  fromString = ipAddress: { inherit ipAddress; };
  __toString = self: "IN AAAA ${self.ipAddress}";
}
