{ lib }:
with lib; {
  options = {
    name = mkOption {
      type = types.str;
      example = "example.org.";
    };
  };

  fromString = name: { inherit name; };
  __toString = self: "IN CNAME ${self.name}";
}
