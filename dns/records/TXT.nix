{ lib }:
with lib; {
  options = {
    value = mkOption {
      type = types.str;
      example = "v=spf1 -all";
    };
  };

  fromString = value: { inherit value; };
  __toString = self: "IN TXT \"${self.value}\"";
}
