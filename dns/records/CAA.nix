{ lib }:
with lib; {
  options = {
    issuerCritical = mkOption {
      type = types.bool;
      default = false;
    };

    tag = mkOption {
      type = types.enum [ "issue" "issuewild" "iodef" "contactemail" "contactphone" ];
    };

    value = mkOption {
      type = types.str;
      example = "letsencrypt.org";
    };
  };

  fromString = value: {
    inherit value;
    tag = "issue";
    issuerCritical = false;
  };
  __toString = self: "IN CAA ${if self.issuerCritical then "1" else "0"} ${self.tag} \"${self.value}\"";
}
