{ lib }:
with lib; {
  options = {
    nameServer = mkOption {
      type = types.str;
      example = "ns1.example.org";
    };
  };

  fromString = nameServer: { inherit nameServer; };
  __toString = self: "IN NS ${self.nameServer}";
}
