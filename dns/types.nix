{ lib, ... }:
with lib; let
  recordTypes = genAttrs [ "SOA" "NS" "A" "AAAA" "MX" "CNAME" "CAA" "TXT" ] (type: import (./. + "/records/${type}.nix") { inherit lib; });
  mkRecordType = t: with types; if hasAttr "fromString" t
    then coercedTo str t.fromString (submodule { inherit (t) options; })
    else submodule { inherit (t) options; };

  subdomainOptions = with types; {
    NS = mkOption { type = listOf (mkRecordType recordTypes.NS); default = []; };
    A = mkOption { type = listOf (mkRecordType recordTypes.A); default = []; };
    AAAA = mkOption { type = listOf (mkRecordType recordTypes.AAAA); default = []; };
    CNAME = mkOption { type = nullOr (mkRecordType recordTypes.CNAME); default = null; };
    CAA = mkOption { type = listOf (mkRecordType recordTypes.CAA); default = []; };
    TXT = mkOption { type = listOf (mkRecordType recordTypes.TXT); default = []; };
    MX = mkOption { type = listOf (mkRecordType recordTypes.MX); default = []; };
  };

  recordToString = name: type: rs:
    if isNull rs then
      []
    else if isList rs then
      map (r: "${name} ${recordTypes.${type}.__toString r}") rs
    else recordToString name type [ rs ];


  zoneRecords = types.addCheck (types.submodule {
    options = {
      SOA = mkOption { type = mkRecordType recordTypes.SOA; };
      TTL = mkOption { type = types.ints.u32; default = 300; };
    } // subdomainOptions;
  }) (attrs: hasAttr "SOA" attrs);
  subzoneRecords = types.submodule {
    options = subdomainOptions;
  };

  properZone = types.submodule ({ name, ...}: {
    options = {
      name = mkOption {
        type = types.str;
        default = name;
        description = "The dns name of the zone";
      };

      extraConfig = mkOption {
        type = types.lines;
      };

      allowTransfer = mkOption {
        type = types.listOf types.str;
      };

      records = mkOption {
        type = types.either zoneRecords subzoneRecords;
        default = [];
        description = "DNS records in the zone";
      };

      subdomains = mkOption {
        type = types.attrsOf zone;
      };
    };
  });

  zoneToString = self: let
    records = getAttrs (attrNames subdomainOptions) self.records;
    subdomains = attrValues self.subdomains;
    hasSOA = hasAttr "SOA" self.records;
    name = if hasSOA then "@" else self.name;
  in ''
    ${optionalString hasSOA ''
      ${self.name}. ${recordTypes.SOA.__toString self.records.SOA}
      $TTL ${toString self.records.TTL}
    ''}
    ${concatStringsSep "\n" (flatten
      (mapAttrsToList (recordToString name) records)
    )}
    ${concatStringsSep "\n" (map (d: zoneToString (d // (optionalAttrs (!hasSOA) { name = "${d.name}.${name}"; }))) subdomains)}
  '';

  coerceZone = from: let
    partitioned = partition (name: hasAttr name recordTypes) (attrNames from);
  in {
    records = getAttrs partitioned.right from;
    subdomains = getAttrs (filter (n: !(builtins.elem n ["extraConfig" "allowTransfer"])) partitioned.wrong) from;
    extraConfig = attrByPath ["extraConfig"] "" from;
    allowTransfer = attrByPath ["allowTransfer"] [] from;
  };

  coerceZonetoDns = from: let
    partitioned = partition (name: hasAttr name recordTypes) (attrNames from);
  in (getAttrs partitioned.right from) // {
    subdomains = getAttrs partitioned.wrong from;
  };

  zone = with types; coercedTo (attrsOf anything) coerceZone properZone;
in {
  inherit zone zoneToString;
}
