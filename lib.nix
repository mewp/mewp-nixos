{ pkgs, lib, ... }:
with lib; {
  writeFiles = name: input: let
    writeFile = file: "cp $text_${toString file.index}Path $out/${file.name} && chmod +x $out/${file.name}";
    fileList = mapAttrsToList nameValuePair input;
    files = zipListsWith (fst: snd: snd // {index = fst;}) (range 1 (length fileList)) fileList;
    file_inputs = listToAttrs (map (file: { name = "text_${toString file.index}"; inherit (file) value; }) files);
    command = "mkdir $out\n" + (concatStringsSep "\n" (map writeFile files));
  in pkgs.runCommand name ({
    passAsFile = attrNames file_inputs;

    preferLocalBuild = true;
    allowSubstitutes = false;
  } // file_inputs) command;
}
