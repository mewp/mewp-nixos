{ nixosModules }: {
  machine = { config, pkgs, ... }: {
    imports = [ nixosModules.dns ];

    mewp.dns.zones."example.org" = {
      SOA = {
        nameServer = "ns.example.org";
        adminEmail = "admin@example.org";
        serial = 0;
      };

      NS = [ "ns.example.org." ];
      ns.A = [ "127.0.0.1" ];
    };

    environment.systemPackages = [ pkgs.bind.dnsutils ];
  };

  testScript = ''
    machine.wait_for_unit("bind.service")
    assert "status: NOERROR" in machine.execute("dig @127.0.0.1 SOA example.org")[1]
  '';
}
