{ lib, pkgs, config, ...}: let
  inherit (lib) mkOption mkEnableOption types;
  cfg = config.mewp-nixos.caldav;
in {
  options.mewp-nixos.caldav = {
    enable = mkEnableOption "Caldav service";
    hostName = mkOption {
      type = types.str;
    };

    acmeHost = mkOption {
      type = types.str;
    };

    rights = mkOption {
      type = types.attrs;
      default = {
        root = {
          user = ".+";
          collection = "";
          permissions = "R";
        };
        principal = {
          user = ".+";
          collection = "{user}";
          permissions = "RW";
        };
        calendars = {
          user = ".+";
          collection = "{user}/[^/]+";
          permissions = "rw";
        };
      };
    };
  };

  config = lib.mkIf cfg.enable {
    services.radicale = {
      package = pkgs.radicale.overridePythonAttrs (old: {
        propagatedBuildInputs = old.propagatedBuildInputs ++ [ pkgs.radicale-dovecot-auth ];
      });
      enable = true;
      rights = cfg.rights;
      settings = {
        auth = {
          type = "radicale_dovecot_auth";
          auth_socket = "/run/radicale-dovecot.sock";
        };
      };
    };

    systemd.services.radicale.serviceConfig.RestrictAddressFamilies = [ "AF_UNIX" ];

    mewp-nixos.dovecot.sockets.auth."/run/radicale-dovecot.sock" = {
      user = "radicale";
      group = "radicale";
    };

    services.nginx = {
      enable = true;
      virtualHosts.${cfg.hostName} = {
        forceSSL = true;
        useACMEHost = cfg.acmeHost;
        locations."/" = {
          proxyPass = "http://localhost:5232";
        };
      };
    };
  };
}
