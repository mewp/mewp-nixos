{ dns }:
{ lib, pkgs, config, ...}:

with lib;
let
  cfg = config.mewp-nixos.mailserver;
  hostname = if isNull cfg.hostname then config.networking.fqdn else cfg.hostname;
  domain = if isNull cfg.domain then hostname else cfg.domain;
  destinations = if isNull cfg.destinations then domain else "${concatStringsSep "," cfg.destinations}";

  spfConfig = pkgs.writeText "spf.conf" "";

  submissionHeaderCleanupRules = pkgs.writeText "submission_header_cleanup_rules" (''
     # Removes sensitive headers from mails handed in via the submission port.
     # See https://thomas-leister.de/mailserver-debian-stretch/
     # Uses "pcre" style regex.

     /^Received:/            IGNORE
     /^X-Originating-IP:/    IGNORE
     /^X-Mailer:/            IGNORE
     /^User-Agent:/          IGNORE
     /^X-Enigmail:/          IGNORE

     # Replaces the user submitted hostname with the server's FQDN to hide the
     # user's host or network.

     /^Message-ID:\s+<(.*?)@.*?>/ REPLACE Message-ID: <$1@${hostname}>
  '');

in {
  imports = [ ./sieve.nix ./rspamd.nix ./dovecot.nix ];

  options.mewp-nixos.mailserver = {
    enable = mkEnableOption "Mewp's mailserver configuration";
    enableACME = mkEnableOption "Generate and use an acme certificate";

    checkSpf = mkOption {
      type = types.bool;
      default = true;
      description = "Whether to enable checking spf records. Disable if behind a NAT.";
    };

    hostname = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "Hostanme to use. Leave null to use the system hostname. It should be an FQDN.";
    };

    roundcube_hostname = mkOption {
      type = types.nullOr types.str;
      default = null;
    };

    hostnames = mkOption {
      type = types.listOf types.str;
      default = [];
      description = "Domains (other than hostname) to get an ACME certificate for.";
    };

    domain = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "The primary domain to use. Set null to use the same value as hostname";
    };

    destinations = mkOption {
      type = types.nullOr (types.listOf types.str);
      default = null;
      description = "Destinations to accept mail for. Defaults to a single domain from the domain attribute.";
    };

    relay_for = mkOption {
      type = types.listOf types.str;
      default = [];
      example = ["1.2.3.4/32"];
      description = "List of networks that are allowed to send mail through this server unauthenticated.";
    };

    recipientBlacklist = mkOption {
      type = types.listOf types.str;
      default = [];
      example = ["someone@example.org"];
      description = "List of email addresses, or domains, to reject mail to.";
    };

    aliases = mkOption {
      type = types.attrsOf types.str;
      default = {};
      example = { "a@example.org" = "b@example.org"; };
      description = "Aliases, put in postfix's virtual_alias_maps";
    };
  };

  config = mkIf cfg.enable {
    mewp-nixos.acme."${hostname}" = if cfg.enableACME && !config.security.acme.acceptTerms then
      abort "You have to set security.acme.{acceptTerms,email} in your configuration before using mewp-mailserver."
      else mkIf cfg.enableACME cfg.hostnames;

    users.groups.acme.members = [ "nginx" ];

    services.opendkim = {
      enable = true;
      domains = "csl:${destinations}";
      selector = "default";
      socket = "inet:8816@localhost";
      user = "postfix";
    };

    services.postfix = rec {
      inherit hostname domain;

      enable = true;
      enableSmtp = true;
      enableSubmission = true;
      enableSubmissions = true;
      submissionOptions = {
        smtpd_tls_security_level = "encrypt";
        smtpd_sasl_auth_enable = "yes";
        smtpd_sasl_path = "private/auth";
        smtpd_sasl_type = "dovecot";
        smtpd_client_restrictions = "permit_sasl_authenticated,reject";
        smtpd_recipient_restrictions = "";
        milter_macro_daemon_name = "ORIGINATING";
        cleanup_service_name = "submission-header-cleanup";
      };
      submissionsOptions = submissionOptions;
      destination = [];
      sslKey = "/var/lib/acme/${hostname}/key.pem";
      sslCert = "/var/lib/acme/${hostname}/fullchain.pem";
      recipientDelimiter = "+";
      networks = cfg.relay_for;
      virtual =  lib.concatStrings (lib.mapAttrsToList (from: to: "${from} ${to}\n") cfg.aliases);
      config = {
        smtpd_client_restrictions = "reject_unknown_helo_hostname";
        smtpd_sender_restrictions = "reject_unknown_sender_domain";
        smtpd_relay_restrictions = "permit_mynetworks,permit_sasl_authenticated,reject_unauth_destination";
        smtpd_recipient_restrictions = "check_recipient_access hash:/var/lib/postfix/conf/recipient_access${optionalString cfg.checkSpf ",check_policy_service unix:private/policy-spf"}";
        virtual_mailbox_domains = destinations;
        virtual_transport = "lmtp:unix:private/dovecot-lmtp";
        milter_default_action = "accept";
        smtpd_milters = [ "inet:localhost:8816" ];
        non_smtpd_milters = [ "inet:localhost:8816" ];
        message_size_limit = toString (50*1024*1024);
      };

      masterConfig = {
        "policy-spf" = {
          type = "unix";
          privileged = true;
          chroot = false;
          command = "spawn";
          args = [ "user=nobody" "argv=${pkgs.pypolicyd-spf}/bin/policyd-spf" "${spfConfig}"];
        };

        "submission-header-cleanup" = {
          type = "unix";
          private = false;
          chroot = false;
          maxproc = 0;
          command = "cleanup";
          args = ["-o" "header_checks=pcre:${submissionHeaderCleanupRules}"];
        };
      };

      mapFiles.recipient_access = pkgs.writeText "recipient_access" (concatStringsSep "\n" (
        map (addr: "${addr} REJECT") cfg.recipientBlacklist
      ));
    };

    mewp-nixos.dovecot = {
      enable = true;
      acmeHost = mkIf cfg.enableACME hostname;
      sockets = {
        lmtp."/var/lib/postfix/queue/private/dovecot-lmtp" = {
          user = "postfix";
          group = "postfix";
        };

        auth."/var/lib/postfix/queue/private/auth" = {
          user = "postfix";
          group = "postfix";
        };
      };
    };

    services.roundcube = mkIf (cfg.roundcube_hostname != null) {
      enable = true;
      hostName = cfg.roundcube_hostname;
    };

    services.rspamd = {
      enable = true;
      postfix.enable = true;
    };

    environment.systemPackages = [ pkgs.dovecot_pigeonhole ];

    networking.firewall.allowedTCPPorts = [ 25 143 443 465 587 ];
  };
}
