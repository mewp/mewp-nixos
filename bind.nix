{ config, lib, pkgs, ... }:

with lib;
let 
  cfg = config.services.bind // {
    zones = map (zone: zone // { file = "/var/db/bind/${zone.name}"; }) config.services.bind.zones;
  };
  super = import "${pkgs.path}/nixos/modules/services/networking/bind.nix" {
    config = config // {
      services.bind = cfg;
    };
    inherit lib pkgs;
  };
in super // {
  config = mkIf cfg.enable (super.config.content // {
    systemd.services.bind = super.config.content.systemd.services.bind // {
      preStart =
      ''mkdir -m 0755 -p /var/db/bind
      chown named /var/db/bind
      ${lib.concatMapStrings (zone: "[ -e /var/db/bind/${zone.name} ] && rm /var/db/bind/${zone.name}\n") config.services.bind.zones}
      ${lib.concatMapStrings (zone: "cp ${zone.file} /var/db/bind/${zone.name}\n") config.services.bind.zones}
      ${super.config.content.systemd.services.bind.preStart}'';
    };
  });
}
