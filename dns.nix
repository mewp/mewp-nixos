{ pkgs, config, lib, ... }:
with lib; let
  cfg = config.mewp.dns;
  dnsTypes = import ./dns/types.nix { inherit lib; };
  generateSerialFor = mapAttrsToList (_: zone: isNull zone.SOA.serial) cfg.zones;

  zoneFiles = mapAttrs (name: value: pkgs.writeText name (dnsTypes.zoneToString value)) cfg.zones;
in {
  options.mewp.dns = {
    zones = mkOption {
      type = types.attrsOf dnsTypes.zone;
      default = {};
    };
  };

  config = mkIf (length (attrNames cfg.zones) > 0) {
    services.bind = {
      enable = true;
      zones = mapAttrs (name: zone: {
        file = "/var/db/bind/${name}";
        master = true;
        slaves = zone.allowTransfer;
        inherit (zone) extraConfig;
      }) cfg.zones;
    };

    system.activationScripts.bind = {
      deps = ["users"];
      text = ''
        SERIAL=$(date +%s)
        install -o named -d /var/db/bind
        ${lib.replaceStrings ["tsig-keygen"] ["${pkgs.bind}/sbin/tsig-keygen"] (lib.readFile ./init-bind-secrets)}
        ${concatStringsSep "\n" (mapAttrsToList (name: zone: ''
            ${pkgs.gnused}/bin/sed -r 's/IN\sSOA\s(\S+)\s(\S+)\s\([0-9]+/IN SOA \1 \2 ('$SERIAL'/' ${zoneFiles.${name}} > /var/db/bind/${escape [ "*" ] name}
            [ -e /var/db/bind/${escape [ "*" ] name}.jnl ] && rm /var/db/bind/${escape [ "*" ] name}.jnl
          '') cfg.zones)}
      '';
    };

    services.bind = {
      extraConfig = ''
          include "/var/lib/secrets/dnskeys.conf";
      '';
    };

    networking.firewall.allowedTCPPorts = [ 53 ];
    networking.firewall.allowedUDPPorts = [ 53 ];
  };
}
